package com.example.flickrollin

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.OnScrollListener

class MainActivity : AppCompatActivity() {
    private var loading = true
    private var page = 0
    private var restApi = ApiClient()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val recyclerView: RecyclerView = findViewById(R.id.recyclerView)
        val layoutManager = GridLayoutManager(this, 2)

        val recyclerAdapter = RecyclerAdapter(this)

        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = recyclerAdapter
        recyclerView.addOnScrollListener(object : OnScrollListener(){
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                val linearLayoutManager = recyclerView.layoutManager as LinearLayoutManager?

                if (!loading) {
                    if (linearLayoutManager?.findLastVisibleItemPosition() == recyclerAdapter.itemCount - 1) {
                        loadPhoto(recyclerAdapter)
                        loading = true
                    }
                }
            }
        })

        loadPhoto(recyclerAdapter)
    }

    private fun loadPhoto(photoList: RecyclerAdapter) {
        page++

        restApi.getPhotos(page) {
            it?.photosList?.photoList?.let { list ->
                photoList.addPhotos(list)
            }
            loading = false
        }
    }
}
