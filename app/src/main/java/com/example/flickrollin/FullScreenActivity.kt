package com.example.flickrollin

import android.os.Bundle
import android.widget.ImageView
import androidx.fragment.app.FragmentActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

class FullScreenActivity : FragmentActivity() {

    companion object {
        const val INTENT_URL = "url"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fullscreen_activity)

        val fullscreenView: ImageView = findViewById(R.id.fullscreen_view)

        val photo = intent.getStringExtra(INTENT_URL).orEmpty()

        Glide.with(this)
                .load(photo)
                .apply(RequestOptions.fitCenterTransform())
                .into(fullscreenView)
    }
}