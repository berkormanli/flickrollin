# FlickRollin

FlickRollin is an Android app which shows the user the most recent photos uploaded on Flickr.

## Info
- FlickRollin is a basic application which shows recent photos of Flickr, which in an infinite scrolling manner.
> With every request, only 20 photos requested from the Flickr API.
- User can view the photo fullscreen by touching on the thumbnail of a photo.

# Dependencies & Used Technologies

## Dependencies
- Retrofit2
- Glide
- RecyclerView v7

## Used Technologies
- Kotlin 1.3.72
- Gradle 4
- JSON2Kotlin (https://www.json2kotlin.com)
