package com.example.flickrollin

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiInterface {
    @GET("services/rest")
    fun getPhotos(
            @Query("page") page: Int,
            @Query("method") method: String,
            @Query("api_key") apiKey: String,
            @Query("extras") extras: String,
            @Query("per_page") perPage: Int,
            @Query("format") format: String,
            @Query("nojsoncallback") noJsonCallback: Int
    ): Call<DataResponse>
}