package com.example.flickrollin

import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.Response as RetrofitResponse

class ApiClient {
    private val apiInterface: ApiInterface

    init {
        val build = Retrofit.Builder()
                .baseUrl("https://www.flickr.com")
                .addConverterFactory(GsonConverterFactory.create())
                .client(OkHttpClient.Builder().build())
                .build()

            apiInterface = build.create(ApiInterface::class.java)
    }
    fun getPhotos(page: Int, onResult: (DataResponse?) -> Unit) {
        apiInterface.getPhotos(
                page,
                "flickr.photos.getRecent",
                "3f8ac6e91cf4f30ebba25c045b7dc8b7",
                "url_sq,url_t,url_s,url_q,url_m,url_n,url_z,url_c,url_l,url_o",
                20,
                "json",
                1
        ).enqueue(
                object : Callback<DataResponse> {
                    override fun onFailure(call: Call<DataResponse>, t: Throwable) {
                        onResult(null)
                    }

                    override fun onResponse(
                            call: Call<DataResponse>,
                            response: RetrofitResponse<DataResponse>
                    ) {
                        val addedUser = response.body()

                        onResult(addedUser)
                    }
                }
        )
    }
}