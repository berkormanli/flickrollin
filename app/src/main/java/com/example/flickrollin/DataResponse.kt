package com.example.flickrollin

import com.google.gson.annotations.SerializedName

data class DataResponse (
        @SerializedName("stat")
        val status: String,

        @SerializedName("photos")
        var photosList: PhotoData
)


data class PhotoData (
    @SerializedName("page")
    val pageNum : Int,

    @SerializedName("pages")
    val pages : Int,

    @SerializedName("perpage")
    val perPage : Int,

    @SerializedName("total")
    val total : Int,

    @SerializedName("photo")
    var photoList: List<Photo>
)

data class Photo (
        @SerializedName("id")
        val id: String,

        @SerializedName("owner")
        val owner: String,

        @SerializedName("secret")
        val secret: String,

        @SerializedName("server")
        val server: Int,

        @SerializedName("farm")
        val farm: Int,

        @SerializedName("title")
        val title: String,

        @SerializedName("ispublic")
        val ispublic: Int,

        @SerializedName("isfriend")
        val isfriend: Int,

        @SerializedName("isfamily")
        val isfamily: Int,

        @SerializedName("url_sq")
        val url_sq: String,

        @SerializedName("height_sq")
        val height_sq: Int,

        @SerializedName("width_sq")
        val width_sq: Int,

        @SerializedName("url_t")
        val url_t: String?,

        @SerializedName("height_t")
        val height_t: Int?,

        @SerializedName("width_t")
        val width_t: Int?,

        @SerializedName("url_s")
        val url_s: String?,

        @SerializedName("height_s")
        val height_s: Int?,

        @SerializedName("width_s")
        val width_s: Int?,

        @SerializedName("url_q")
        val url_q: String?,

        @SerializedName("height_q")
        val height_q: Int?,

        @SerializedName("width_q")
        val width_q: Int?,

        @SerializedName("url_m")
        val url_m: String?,

        @SerializedName("height_m")
        val height_m: Int?,

        @SerializedName("width_m")
        val width_m: Int?,

        @SerializedName("url_n")
        val url_n: String?,

        @SerializedName("height_n")
        val height_n: Int?,

        @SerializedName("width_n")
        val width_n: Int?,

        @SerializedName("url_z")
        val url_z: String?,

        @SerializedName("height_z")
        val height_z: Int?,

        @SerializedName("width_z")
        val width_z: Int?,

        @SerializedName("url_c")
        val url_c: String?,

        @SerializedName("height_c")
        val height_c: Int?,

        @SerializedName("width_c")
        val width_c: Int?,

        @SerializedName("url_l")
        val url_l: String?,

        @SerializedName("height_l")
        val height_l: Int?,

        @SerializedName("width_l")
        val width_l: Int?,

        @SerializedName("url_o")
        val url_o: String?,

        @SerializedName("height_o")
        val height_o: Int?,

        @SerializedName("width_o")
        val width_o: Int?
    )

fun Photo.bigUrl(): String {
    url_o?.let {
        return it
    }

    url_l?.let {
        return it
    }

    url_c?.let {
        return it
    }

    return url_sq
}