package com.example.flickrollin

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.bumptech.glide.Glide
import com.example.flickrollin.FullScreenActivity.Companion.INTENT_URL
import com.example.flickrollin.RecyclerAdapter.FRViewHolder

class RecyclerAdapter(private val context: Context) : RecyclerView.Adapter<FRViewHolder>() {

    private val photosList: MutableList<Photo> = ArrayList();

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FRViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.photo_layout, parent, false)
        return FRViewHolder(view)
    }

    override fun onBindViewHolder(holder: FRViewHolder, position: Int) {
        val photo = photosList[position]

        Glide.with(context).load(photo.url_sq).into(holder.Album)

        holder.Album.setOnClickListener {
            val intent = Intent(context, FullScreenActivity::class.java).apply{
                putExtra(INTENT_URL, photosList[position].bigUrl())
                addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            }

            context.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return photosList.size
    }

    class FRViewHolder(itemView: View) : ViewHolder(itemView) {
        var Album: ImageView = itemView.findViewById(R.id.album_image)
    }

    fun addPhotos(Photo: List<Photo>) {
        this.photosList.addAll(Photo)
        notifyDataSetChanged()
    }

}